package main

import (
 "os"
 "syscall"
)

func main() {
 o := os.NewFile(uintptr(syscall.Stdout), "/dev/stdout")
 defer o.Close()

 for bits := 4946144450195624; bits > 0; bits >>= 5 {
  char := string(((bits&31 | 64) % 95) + 32)
  o.Write([]byte(char))
 }
}